#!/bin/bash

set -e

VERSION=0.0.2

GITHUBURL=https://github.com/highb/pathspec-ruby/archive/${VERSION}.zip


# download zipball
if [[ ! -f pathspec-ruby-$VERSION.zip ]]; then
    curl -o pathspec-ruby-$VERSION.zip -L $GITHUBURL
fi

# extract zipball
[[ -d pathspec-ruby-$VERSION ]] && rm -r pathspec-ruby-$VERSION
unzip pathspec-ruby-$VERSION.zip

pushd pathspec-ruby-$VERSION
  # repack
  tar -cJvf pathspec-ruby-$VERSION-tests.tar.xz spec
  mv pathspec-ruby-$VERSION-tests.tar.xz ..
popd

# Clean up
rm pathspec-ruby-$VERSION.zip
rm -r pathspec-ruby-$VERSION
