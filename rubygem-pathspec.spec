%global gem_name pathspec

Name:           rubygem-%{gem_name}
Version:        0.0.2
Release:        1%{?dist}
Summary:        Use to match path patterns such as gitignore

License:        ASL 2.0
URL:            https://rubygems.org/gems/%{gem_name}
Source0:        https://rubygems.org/downloads/%{gem_name}-%{version}.gem
Source1:        rubygem-pathspec-generate-tarball.sh
Source2:        pathspec-ruby-%{version}-tests.tar.xz
BuildArch:      noarch

BuildRequires:  rubygems-devel
BuildRequires:  rubygem(rspec)
BuildRequires:  rubygem(fakefs)

%description
Use to match path patterns such as gitignore.


%package doc
Summary: Documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}.


%prep
gem unpack %{SOURCE0}
%setup -q -D -T -n  %{gem_name}-%{version} -a 2
gem spec %{SOURCE0} -l --ruby > %{gem_name}.gemspec

%if 0%{fedora} < 22
  # Fedora 21 has Rspec 2.x, and Fedora 22 has Rspec 3.x.
  # Switch to the older Rspec functions.
  for f in $(find spec -type f); do
    sed -i $f \
      -e "s/is_expected\.to/should/g" \
      -e "s/is_expected\.not_to/should_not/g"
  done
%endif

%build
gem build %{gem_name}.gemspec
%gem_install


%install
mkdir -p %{buildroot}%{gem_dir}
cp -a ./%{gem_dir}/* %{buildroot}%{gem_dir}/

%check
cp -pr spec .%{gem_instdir}
pushd .%{gem_instdir}
  rspec -Ilib spec
  rm -r spec/
popd

%files
%license LICENSE
%dir %{gem_instdir}
%license %{gem_instdir}/LICENSE
%doc %{gem_instdir}/CHANGELOG.md
%doc %{gem_instdir}/README.md
%{gem_libdir}
%exclude %{gem_cache}
%{gem_spec}

%files doc
%doc %{gem_docdir}


%changelog
* Fri Apr 10 2015 Orion Poplawski <orion@cora.nwra.com> - 0.0.2-1
- Initial package
